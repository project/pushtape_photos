<?php
/**
 * @file
 * pushtape_photos.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function pushtape_photos_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu:photos
  $menu_links['main-menu:photos'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'photos',
    'router_path' => 'photos',
    'link_title' => 'Photos',
    'options' => array(),
    'module' => 'system',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Photos');


  return $menu_links;
}
